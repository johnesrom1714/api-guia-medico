import { Injectable } from "@nestjs/common";
import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import { PrestadorRepository } from "src/core/repositories/prestador-repository";

@ValidatorConstraint({ name: 'ContemTipoPrestador', async: true })
@Injectable()
export class ContemTipoPrestador implements ValidatorConstraintInterface {

  constructor(private prestadorRepository: PrestadorRepository) {}

  async validate(value: string) {
    
    console.log("==========> ", value);
    const result = await this.prestadorRepository.getTiposById(value);
    try {
      


      console.log("=> ", result);

        if (result) {
                    return true;
        }

      return false;
      
    } catch (e) {
        console.log(e.message)
      return false;
    }


  }

  defaultMessage(args: ValidationArguments) {
    return `Tipo prestador não existe`;
  }

}