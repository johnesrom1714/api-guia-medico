import { Controller, Get, Param, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ProdutoModel } from '../../models/produto.model';
import { EnderecoPrestadorRepository } from 'src/core/repositories/endereco-prestador-repository';
import { EnderecoModel } from '../../models/endereco.model';
import { EstadoModel } from '../../models/estado.model';
import { CidadeModel } from '../../models/cidade.model';
import { BairroModel } from '../../models/bairro.model';

@ApiTags('Informações sobre endereco')
@Controller('endereco-prestador')
export class EnderecoController {
  
  constructor(
      private enderecoPrestador: EnderecoPrestadorRepository,
  ) {}

  @Get('/estados')
  @ApiResponse({
    status: 200,
    description: 'Lista estados vinculado a prestadores',
    type: EstadoModel,
    isArray: true
  })
  async getEstados() : Promise<ProdutoModel[]>{
    return await this.enderecoPrestador.getEstados();
  }

  @Get('/cidades/:uf')
  @ApiResponse({
    status: 200,
    description: 'Lista cidades vinculado a estado',
    type: CidadeModel,
    isArray: true
  })
  async getCidades(@Param('uf') uf : string) {
    return await this.enderecoPrestador.getCidadeByEstado(uf);
  }

  @Get('/bairros')
  @ApiResponse({
    status: 200,
    description: 'Lista neirros vinculado a cidade',
    type: BairroModel,
    isArray: true
  })
  async getBairros(@Query('cidade') cidade : string) {
    return await this.enderecoPrestador.getBairrosByCidade(cidade);
  }

}
