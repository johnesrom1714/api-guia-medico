import { Test, TestingModule } from '@nestjs/testing';
import { GuiaRedeController } from './guia-rede.controller';

describe('GuiaRedeController', () => {
  let controller: GuiaRedeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GuiaRedeController],
    }).compile();

    controller = module.get<GuiaRedeController>(GuiaRedeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
