import { Controller, Get, HttpException, Inject, Param, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { RedeRepository } from 'src/core/repositories/rede-repository';
import { PlanoRepository } from 'src/core/repositories/plano-repository';

import { EnderecoDTO } from '../../dto/endereco-dto';
import { EspecialidadeDTO } from '../../dto/especialidade-dto';
import { PrestadorDTO } from '../../dto/prestador-dto';
import { RedeDTO } from '../../dto/rede-dto';
import { TipoPrestadorDTO } from '../../dto/tipo-prestador-dto';
import { EnderecoModel } from '../../models/endereco.model';
import { EspecialidadeModel } from '../../models/especialidade.model';
import { ProdutoModel } from '../../models/produto.model';
import { RedeModel } from '../../models/rede.model';
import { TipoPrestadorModel } from '../../models/tipo_prestador.model';
import { PrestadorRepository } from 'src/core/repositories/prestador-repository';
import { EspecialidadeRepository } from 'src/core/repositories/especlialidade-repository copy';
import { Address } from 'src/models/address.mode';
import { AddressInterface } from 'src/core/repositories/address/address.interface';

@ApiTags('Guia de rede')
@Controller('guia')
export class GuiaRedeController {
  
  constructor(
      private planoRepository: PlanoRepository,
      private redeRepository: RedeRepository,
      private prestadorRepository: PrestadorRepository,
      private especialidadeRepository: EspecialidadeRepository,
      @Inject('ADDRESS_REPOSITORY') 
      private address: AddressInterface
  ) {}

  @Get('/produtos')
  @ApiResponse({
    status: 200,
    description: 'Lista os produtos/planos',
    isArray: true,
    type: ProdutoModel,
  })
  async getPlans() : Promise<ProdutoModel[]>{
    return await this.planoRepository.getActives();
  }

  @Get('/redes')
  @ApiResponse({
    status: 200,
    description: 'Lista a Rede(s) do produto selecionado',
    isArray: true,
    type: RedeModel,
  })
  async getRede(@Query() query: RedeDTO) : Promise<RedeModel[]> {
    return this.redeRepository.getAll();
  }

  @Get('/tipo_prestador')
  @ApiResponse({
    status: 200,
    description: 'Lista os tipos de prestadores',
    type: TipoPrestadorModel,
  })
  async getTipoPrestador(@Query() query: TipoPrestadorDTO) : Promise<TipoPrestadorModel[]> {
    return this.prestadorRepository.getTipos();
  }

  @Get('/tipo_prestador/:id')
  @ApiResponse({
    status: 200,
    description: 'Lista os tipos de prestadores',
    type: TipoPrestadorModel,
  })
  async getTipoPrestadorById(@Param('id') id: string) : Promise<TipoPrestadorModel> {
    
    const result = await this.prestadorRepository.getTiposById(id);
    
    if(result) return result;
    
    throw new HttpException(
      "Tipo de prestador não encontrado",
      404
    );

  }

  @Get('/especialidades')
  @ApiResponse({
    status: 200,
    description: 'Lista as especialidades',
    type: EspecialidadeModel,
    isArray: true,
  })
  async getEspecialidades(@Query() query: EspecialidadeDTO) : Promise<EspecialidadeModel[]> {
    return this.especialidadeRepository.getAll();
  }

  
  @Get('/endereco/:cep')
  @ApiResponse({
    description: 'Busca um Endereço pelo CEP',
    status: 200,
    type: EnderecoModel,
  })
  async getEnderecoCep(@Param('cep') cep: number) : Promise<Address> {
    return this.address.fetchAddress(cep);
  }


  @Get('/prestadores')
  @ApiResponse({
    isArray: true,
    type: EnderecoDTO,
    description: 'Lista os prestadores com os filtros selecionados',
    status: 200,
  })
  @UsePipes(new ValidationPipe({ transform: true }))
  getPrestador(@Query() query: PrestadorDTO) {
    return this.prestadorRepository.fetchProvidersByGeolocation(query);
  }
}
