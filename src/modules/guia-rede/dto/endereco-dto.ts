import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString, Max, Min, Validate } from 'class-validator';
import { ContemTipoPrestador } from '../validation/dto/contem-tipo-prestador';

export class EnderecoDTO {
  @ApiProperty({ description: 'Latitude' })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  latitude: number;

  @ApiProperty({ description: 'Longitude' })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  longitude: number;

  @ApiProperty({ description: 'Distância em quilômetro' })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  @Max(200)
  distancia: number = 15;

  @ApiProperty({ description: 'ID Especialidade' })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  especialidade_id;

  @ApiProperty({ description: 'ID Plano' })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  plano_id;

  @ApiProperty({ description: 'ID Rede' })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  rede_id;
  

  @ApiProperty({ description: 'Tipo prestador' })
  @IsOptional()
  @IsString()
  // @Validate(ContemTipoPrestador)
  @Type(() => String)
  tipo_prestador_id;


}
