import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class EspecialidadeDTO {
  @ApiProperty({ description: 'ID do tipo de prestador' })
  @IsString()
  @IsNotEmpty()
  tipo_prestador_id: string;

  @ApiProperty({ description: 'ID da rede' })
  @IsString()
  @IsNotEmpty()
  rede_id: string;
}
