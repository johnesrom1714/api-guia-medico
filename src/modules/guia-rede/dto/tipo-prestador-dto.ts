import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class TipoPrestadorDTO {
  @ApiProperty({ description: 'ID da rede.' })
  @IsString()
  @IsNotEmpty()
  rede_id: string;
}
