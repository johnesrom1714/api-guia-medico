import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class RedeDTO {
  @ApiProperty({ description: 'ID do produto.' })
  @IsString()
  @IsNotEmpty()
  produto_id: string;
}
