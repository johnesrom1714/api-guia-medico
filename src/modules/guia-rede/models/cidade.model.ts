import { ApiProperty } from '@nestjs/swagger';

export class CidadeModel {
  @ApiProperty()
  cidade: string;
}
