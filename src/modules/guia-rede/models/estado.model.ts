import { ApiProperty } from '@nestjs/swagger';

export class EstadoModel {
  @ApiProperty()
  uf: string;
}
