import { ApiProperty } from '@nestjs/swagger';

export class TipoPrestadorModel {
  @ApiProperty({ description: 'ID do tipo de prestador' })
  id: number;

  @ApiProperty({ description: 'Descricao do tipo de prestador' })
  descricao: string;
}
