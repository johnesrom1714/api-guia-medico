import { ApiProperty } from '@nestjs/swagger';

export class PrestadorEnderecoModel {
  @ApiProperty()
  cep: string;

  @ApiProperty()
  uf: string;

  @ApiProperty()
  cidade: string;

  @ApiProperty()
  bairro: string;

  @ApiProperty()
  tipo_logradouro: string;

  @ApiProperty()
  logradouro: string;

  @ApiProperty()
  numero: number;

  @ApiProperty()
  complemento: string;
}
