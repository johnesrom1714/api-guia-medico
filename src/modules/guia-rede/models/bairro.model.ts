import { ApiProperty } from '@nestjs/swagger';

export class BairroModel {
  @ApiProperty()
  bairro: string;
}
