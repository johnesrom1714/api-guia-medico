import { ApiProperty } from '@nestjs/swagger';

export class ConselhoModel {
  @ApiProperty()
  sigla: string;

  @ApiProperty()
  uf: string;

  @ApiProperty()
  numero: number;
}
