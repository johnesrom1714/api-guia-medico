import { ApiProperty } from '@nestjs/swagger';

export class RedeModel {
  @ApiProperty({ description: 'ID da rede' })
  id: number;

  @ApiProperty({ description: 'Nome da rede' })
  nome: string;
}
