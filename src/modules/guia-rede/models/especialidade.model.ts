import { ApiProperty } from '@nestjs/swagger';

export class EspecialidadeModel {
  @ApiProperty({ description: 'ID da especialidade' })
  id: number;

  @ApiProperty({ description: 'Nome da especialidade' })
  nome: string;
}
