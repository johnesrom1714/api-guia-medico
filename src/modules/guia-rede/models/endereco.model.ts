import { ApiProperty } from '@nestjs/swagger';

export class EnderecoModel {
  @ApiProperty()
  cep: string;
}
