import { ApiProperty } from '@nestjs/swagger';
import { PrestadorEnderecoModel } from './prestador-endereco.model';

export class LocalAtendimentoModel {
  @ApiProperty()
  id: number;

  @ApiProperty()
  nome: string;

  @ApiProperty()
  latitude: number;

  @ApiProperty()
  longitude: number;

  @ApiProperty()
  telefone: string;

  @ApiProperty()
  endereco: PrestadorEnderecoModel;
}
