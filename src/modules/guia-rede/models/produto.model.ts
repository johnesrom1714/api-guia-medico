import { ApiProperty } from '@nestjs/swagger';

class Planos {

  @ApiProperty({ description: 'Codigo do plano' })
  codigo: string;

  @ApiProperty({ description: 'Nome do plano' })
  nome:  string;
  
}

class Certificados {

  @ApiProperty({ description: 'ID' })
  id: string;

  @ApiProperty({ description: 'Nome da certificação' })
  certificacao:  string;
  
}

class Especialidade {

  @ApiProperty({ description: 'Id do plano' })
  id: string;

  @ApiProperty({ description: 'Nome do plano' })
  nome:  string;
  
}

export class ProdutoModel {
  @ApiProperty({ description: 'ID do plano' })
  id: number;

  @ApiProperty({ description: 'Nome do plano' })
  nome: string;

  @ApiProperty({ description: 'Nome fantasia' })
  nome_fantasia: string;

  @ApiProperty({ description: 'Código do plano' })
  codigo: number;

  @ApiProperty({ description: 'CNPJ' })
  cnpj: string;

  @ApiProperty({ description: 'Bairro' })
  bairro: string;

  @ApiProperty({ description: 'Endereço' })
  endereco: string;

  @ApiProperty({ description: 'Número' })
  numero: number;

  @ApiProperty({ description: 'Cidade' })
  cidade: string;

  @ApiProperty({ description: 'Estado' })
  estado: string;

  @ApiProperty({ description: 'CEP' })
  cep: string;

  @ApiProperty({ description: 'Telefone' })
  telefone: number;

  @ApiProperty({ description: 'Distancia' })
  distancia: number;

  @ApiProperty({ description: 'Registro ANS do plano' })
  registro_ans: number;

  @ApiProperty({ description: 'Registro ANS do plano' })
  certificacao: [];

  @ApiProperty({ description: 'Segmentação do plano' })
  segmentacao: string;

  @ApiProperty({ 
    isArray: true,
    type: Planos,
    description: 'Segmentação do plano' 
  })
  planos;

  @ApiProperty({ 
    isArray: true,
    type: Certificados,
    description: 'Certificados do plano' 
  })
  certificados;

  
}


    