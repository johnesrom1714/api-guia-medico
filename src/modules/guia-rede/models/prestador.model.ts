import { ApiProperty } from '@nestjs/swagger';
import { ConselhoModel } from './conselho.model';
import { LocalAtendimentoModel } from './local-atendimento.model';

export class PrestadorModel {
  @ApiProperty()
  id: number;

  @ApiProperty()
  nome: string;

  @ApiProperty()
  especialidade: string;

  @ApiProperty()
  conselho: ConselhoModel;

  @ApiProperty()
  local_atendimento: LocalAtendimentoModel;
}
