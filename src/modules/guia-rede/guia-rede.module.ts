import { Module } from '@nestjs/common';
import { DbModule } from 'src/core/database/db.module';
import { RedeRepository } from 'src/core/repositories/rede-repository';
import { PlanoRepository } from 'src/core/repositories/plano-repository';
import { GuiaRedeController } from './controllers/guia-rede/guia-rede.controller';
import { PrestadorRepository } from 'src/core/repositories/prestador-repository';
import { EspecialidadeRepository } from 'src/core/repositories/especlialidade-repository copy';
import { configAddressProvider } from 'src/core/repositories/address/address.provider';
import { EnderecoPrestadorRepository } from 'src/core/repositories/endereco-prestador-repository';
import { EnderecoController } from './controllers/guia-rede/endereco.controller';

@Module({
  imports:[
    DbModule
  ],
  controllers: [GuiaRedeController, EnderecoController],
  providers: [
    configAddressProvider,
    PlanoRepository,
    RedeRepository,
    PrestadorRepository,
    EnderecoPrestadorRepository,
    EspecialidadeRepository
  ],
  exports: [
    configAddressProvider,
    PlanoRepository,
    RedeRepository,
    PrestadorRepository,
    EnderecoPrestadorRepository,
    EspecialidadeRepository
  ]
})
export class GuiaRedeModule {}
