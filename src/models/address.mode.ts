export interface Address {    
    cep: string;
    logradouro: string;
    complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
    ibge: number;   
    fulladdress: string;  
}

export class Address implements Address{

    getFullAddress() {
        return this.logradouro + ' - ' + this.bairro + ' ' + this.localidade + ' - ' + this.uf;
    }

}