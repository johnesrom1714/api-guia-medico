import { Injectable } from "@nestjs/common";
import { RedeModel } from "src/modules/guia-rede/models/rede.model";
import { DbService } from "../database/services/db/db.service";

@Injectable()
export class RedeRepository {

    constructor(private db: DbService) {}

    async getAll() : Promise<RedeModel[]> {
        let sql = `
            SELECT 
                NNUMEREDEA as id,
                CNOMEREDEA as nome
                FROM SOLUS.HSSREDEA
            WHERE CSITUREDEA <> 'C'
                AND CWEB_REDEA = 'S'
                AND NNUMEREDEA IN(SELECT NNUMEREDEA FROM SOLUS.HSSREDPL)
            ORDER BY 2
        `;
        return await this.db.query<RedeModel>(sql);
    }
}