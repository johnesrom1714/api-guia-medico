import { Injectable } from "@nestjs/common";
import { EspecialidadeModel } from "src/modules/guia-rede/models/especialidade.model";
import { DbService } from "../database/services/db/db.service";

@Injectable()
export class EspecialidadeRepository {

    constructor(private db: DbService) {}

    async getAll() : Promise<EspecialidadeModel[]> {
        let sql = `
            SELECT 
                CNOMEESPEC as id,
                NNUMEESPEC as nome
            FROM SOLUS.HSSESPEC
            WHERE CSITUESPEC = 'A'
                AND CPUBLESPEC = 'S'
            ORDER BY CNOMEESPEC
        `;
        return await this.db.query<EspecialidadeModel>(sql);
    }
}