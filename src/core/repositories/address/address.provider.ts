import { PrestadorRepository } from "../prestador-repository";
import { Viacep } from "./viacep";

export const configAddressProvider = {
    provide: 'ADDRESS_REPOSITORY',
    useClass: Viacep
};
