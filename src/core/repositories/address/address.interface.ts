export interface AddressInterface {

    fetchAddress(cep: number);

    fetchAddressByLatLong(lat: number, long: number);

}