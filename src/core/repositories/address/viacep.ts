import { AddressInterface } from "./address.interface";
import axios from 'axios';
import { HttpException, Injectable, NotFoundException } from "@nestjs/common";
import { Address } from "src/models/address.mode";

@Injectable()
export class Viacep implements AddressInterface {
    
    instance: any;

    constructor() {
        this.instance = axios.create({
            baseURL: 'https://viacep.com.br/',
            timeout: 1000
        });
    }
    
    async fetchAddress(cep: number) : Promise<Address> {

        try {
            const response = await this.findFromExternalService(cep);
            if(response.data.error || !response.data.cep) {
                throw new NotFoundException("CEP inválido");
            }
            return this.hydrate(response.data);
        } catch (e: any) {
            if (e instanceof NotFoundException) {
                throw e;
            } else {
                console.log(e);
                throw new HttpException("Erro ao buscar endereço", 500)
            }
        }
        
    }

    async findFromExternalService(cep: number) {
        return this.instance.get(`/ws/${cep}/json/`);    
    }

    private hydrate(data: any) : Address {
        const address = new Address();
        address.cep = data.cep;
        address.bairro = data.bairro;
        address.complemento = data.complemento;
        address.ibge = data.igbe;
        address.localidade = data.localidade;
        address.logradouro = data.logradouro;
        address.uf = data.uf;
        address.fulladdress = address.getFullAddress();
        return address;
    }

    fetchAddressByLatLong(lat: number, long: number) {
        throw new Error("Method not implemented.");
    }
}