import { Injectable } from "@nestjs/common";
import { ProdutoModel } from "src/modules/guia-rede/models/produto.model";
import { DbService } from "../database/services/db/db.service";

@Injectable()
export class PlanoRepository {

    constructor(private db: DbService) {}

    async getActives() : Promise<ProdutoModel[]> {
        let sql = `
        SELECT DISTINCT HSSPLAN.NNUMEPLAN as id, 
                HSSPLAN.CCODIPLAN as codigo,
                NVL(HSSTITU.CSCPATITU,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN), HSSPLAN.CANTIPLAN)) as registro_ans,
                HSSPLAN.CCODIPLAN || ' - ' || NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) || ' - ' || DECODE(HSSPLAN.CNATUPLAN,1,
                'Individual Familiar',
                3,'Coletivo por Adesão',4,'Coletivo Empresarial',HSSPLAN.CNATUPLAN)|| ' - Registro ANS: ' || NVL(HSSTITU.CSCPATITU,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN),
                HSSPLAN.CANTIPLAN)) || ' - ATIVO' as nome,
                DECODE(HSSPLAN.CNATUPLAN,1,
                    'Individual Familiar',
                    3,'Coletivo por Adesão',4, 'Coletivo Empresarial',HSSPLAN.CNATUPLAN) as segmentacao
            FROM SOLUS.HSSPLAN,SOLUS.HSSTITU
        WHERE NVL(CSITUPLAN,'A') = 'A'
            AND HSSPLAN.CWEB_PLAN ='S'
            AND HSSPLAN.CEXRWPLAN = 'S'
            AND HSSTITU.CSITUTITU = 'A'
            AND 0 < (SELECT COUNT(*) FROM SOLUS.HSSUSUA WHERE CSITUUSUA = 'A' AND NNUMETITU = HSSTITU.NNUMETITU)
            AND HSSPLAN.NNUMEPLAN = HSSTITU.NNUMEPLAN
        ORDER BY 2
        `;
        return await this.db.query<ProdutoModel>(sql);
    }
}