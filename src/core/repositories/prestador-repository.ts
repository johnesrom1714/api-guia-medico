import { Injectable } from "@nestjs/common";
import { PrestadorDTO } from "src/modules/guia-rede/dto/prestador-dto";
import { TipoPrestadorModel } from "src/modules/guia-rede/models/tipo_prestador.model";
import { DbService } from "../database/services/db/db.service";

@Injectable()
export class PrestadorRepository {

    constructor(private db: DbService) {}

    async getTiposById(value: string) : Promise<TipoPrestadorModel> {
        let sql = `            
            SELECT 	DISTINCT CGRUPPRES AS GRUPO_ID, 
            (SELECT CDISPDOMIN
                FROM SOLUS.HSSDOMIN
                WHERE CTIPODOMIN = 'GRUPO_PRESTADOR'
                AND CVALODOMIN = CGRUPPRES
            ) AS GRUPO
            FROM SOLUS.FINPRES
            WHERE ( (CCREDPRES IN ('S','O'))  OR (CCREDPRES IN ('N','M') AND (DCANCPRES + NVL(NDVISPRES, 0)) >= TRUNC(SYSDATE)) )
            and CGRUPPRES = :value
            ORDER BY 2
        `;
        return await this.db.findOne<TipoPrestadorModel>(sql, {value: value});
    }

    async getTipos() : Promise<TipoPrestadorModel[]> {
        let sql = `            
            SELECT 	DISTINCT CGRUPPRES AS GRUPO_ID, 
            (SELECT CDISPDOMIN
                FROM SOLUS.HSSDOMIN
                WHERE CTIPODOMIN = 'GRUPO_PRESTADOR'
                AND CVALODOMIN = CGRUPPRES
            ) AS GRUPO
            FROM SOLUS.FINPRES
            WHERE ( (CCREDPRES IN ('S','O'))  OR (CCREDPRES IN ('N','M') AND (DCANCPRES + NVL(NDVISPRES, 0)) >= TRUNC(SYSDATE)) )
            ORDER BY 2
        `;
        return await this.db.query<TipoPrestadorModel>(sql);
    }

    async fetchProvidersByGeolocation(params: PrestadorDTO) : Promise<any> {

        let filterMap = new Map<string, string>();

        let connection = await this.db.open();
        let binds = {};
        
        binds = {
            latitude: params.latitude, 
            longitude: params.longitude
        };
        
        if (!params.bairro && !params.uf && !params.cidade){
            binds = { ...binds, ...{distancia: params.distancia > 50 ? 50 : params.distancia} };
            filterMap.set('distancia', 'AND a.distancia <= :distancia');
        }
        
        let pagina = 1;

        if (params.pagina) {

            pagina = (params.pagina <= 0) ? 1 : params.pagina;

            binds = { ...binds, ...{pagina: pagina} };
            filterMap.set('pagina', 'OFFSET (:pagina - 1) * :limite');

         }else{
            binds = { ...binds, ...{pagina: 1} };
            filterMap.set('pagina', 'OFFSET (:pagina - 1) * :limite');
         }

        if (params.limite) {
            binds = { ...binds, ...{limite: params.limite} };
            filterMap.set('limite', 'ROWS FETCH NEXT :limite ROWS ONLY');
         }else{
            binds = { ...binds, ...{limite: 10} };
            filterMap.set('limite', 'ROWS FETCH NEXT :limite ROWS ONLY');
         }


        if (params.especialidade_id) {
           binds = { ...binds, ...{especialidade_id: params.especialidade_id} };
           filterMap.set('especialidade_id', 'AND ESP.NNUMEESPEC = :especialidade_id');
        }

        if (params.plano_id) {
           binds = { ...binds, ...{plano_id: params.plano_id} };
           filterMap.set('plano_id', 'AND P.NNUMEPLAN = :plano_id');
        }

        if (params.tipo_prestador_id) {
            binds = { ...binds, ...{tipo_prestador_id: String(params.tipo_prestador_id)} };
            filterMap.set('tipo_prestador_id', 'AND FIN.CGRUPPRES = :tipo_prestador_id');
        }

        if (params.rede_id) {
            binds = { ...binds, ...{rede_id: params.rede_id} };
            filterMap.set('rede_id', `AND PR.NNUMEREDEA = :rede_id
                                      AND PR.CPWEBPREDE = 'S'`);
        }

        if (params.bairro) {
            binds = { ...binds, ...{prestador_bairro: String(params.bairro)} };
            filterMap.set('prestador_bairro', 'AND E.CBAIRENDP = :prestador_bairro');
        }

        if (params.uf) {
            binds = { ...binds, ...{prestador_uf: String(params.uf)} };
            filterMap.set('prestador_uf', 'AND E.CESTAENDP = :prestador_uf');
        }

        if (params.cidade) {
            binds = { ...binds, ...{prestador_cidade: String(params.cidade)} };
            filterMap.set('prestador_cidade', 'AND E.CCIDAENDP = :prestador_cidade');
        }

        // let sql = `            
        //     SELECT
        //     *
        //     FROM (
        //         SELECT
        //             F.NNUMEPRES id, 
        //             F.CNOMEPRES nome,
        //             F.CFANTPRES nome_fantasia,
        //             F.CCGC_PRES cnpj,
        //             E.CBAIRENDP bairro,
        //             E.CENDEENDP endereco,
        //             E.CNUMEENDP numero,
        //             E.CCIDAENDP cidade,
        //             E.CESTAENDP estado,
        //             E.CCEP_ENDP cep,  
        //             F.CFONEPRES telefone,
        //             F.CIDENPRES, 
        //             CAST(SOLUS.CALCULA_DISTACIA_COORD(
        //                         :latitude,
        //                         :longitude,
        //                         E.CLATIENDP,
        //                         E.CLONGENDP) AS NUMBER) distancia
        //             FROM SOLUS.FINPRES F
        //         JOIN SOLUS.HSSENDP E ON E.NNUMEPESS = F.NNUMEPESS                              
        //         WHERE
        //         1=1
        //         AND E.CLATIENDP IS NOT NULL
        //         AND E.CLONGENDP IS NOT NULL
        //         AND F.NNUMEPRES IN (
        //                 SELECT 
        //                     DISTINCT FIN.NNUMEPRES
        //                 FROM
        //                     SOLUS.FINPRES  FIN
        //                     INNER JOIN SOLUS.HSSPREDE PR  ON PR.NNUMEPRES   = FIN.NNUMEPRES
        //                     INNER JOIN SOLUS.HSSREDPL R   ON R.NNUMEREDEA   = PR.NNUMEREDEA
        //                     INNER JOIN SOLUS.HSSPLAN  P   ON P.NNUMEPLAN    = R.NNUMEPLAN 
        //                     LEFT  JOIN SOLUS.HSSESPRE PRE ON PRE.NNUMEPRES  = FIN.NNUMEPRES 
        //                     LEFT  JOIN SOLUS.HSSESPEC ESP ON ESP.NNUMEESPEC = PRE.NNUMEESPEC
        //                 WHERE
        //                     1=1
        //                 ${filterMap.get('especialidade_id') || ''}
        //                 ${filterMap.get('plano_id') || ''}
        //                 ${filterMap.get('tipo_prestador_id') || ''}
        //                 ${filterMap.get('rede_id') || ''}
        //                 ${filterMap.get('prestador_bairro') || ''}
        //                 ${filterMap.get('prestador_uf') || ''}
        //                 ${filterMap.get('prestador_cidade') || ''}
        //                 AND PRE.CPUBLESPRE = 'S'
        //         )
        //     ) a
        //     where 
        //     1=1
        //     ${filterMap.get('distancia') || ''}
        //     order by a.distancia asc
        // `;

        let sql = `	
        SELECT
        
             JSON_OBJECT(
                 KEY 'id,' IS                     x.NNUMEPRES,
                 KEY 'nome' IS                    x.CNOMEPRES,
                 KEY 'nome_fantasia' IS   x.CFANTPRES,
                 KEY 'cnpj' is                    x.CCGC_PRES,
                 KEY 'bairro' IS              x.CBAIRENDP,
                 KEY 'endereco' IS                x.CENDEENDP,
                 KEY 'numero' IS              x.CNUMEENDP,
                 KEY 'cidade' IS              x.CCIDAENDP,
                 KEY 'estado' IS              x.CESTAENDP,
                 KEY 'cep, ' IS               x.CCEP_ENDP,
                 KEY 'telefone' IS                x.CFONEPRES,
                 KEY 'endereco' is                x.CIDENPRES,
                 KEY 'distancia' is               x.distancia,
                  KEY 'certificacao' IS (
                    select 
                        JSON_ARRAYAGG( json_object(x.* RETURNING CLOB) RETURNING CLOB ) 
                    from (
                      select 
                            g.NNUMEPGRAD  id,
                            g.cdescpgrad certificacao
                      from solus.hsspgrad g, solus.finpres ff
                      where g.nnumepres=ff.nnumepres
                      and ff.NNUMEPRES = x.NNUMEPRES 
                    ) x
                ),
                key 'planos' is (
                    select 
                        JSON_ARRAYAGG( json_object(x.* RETURNING CLOB) RETURNING CLOB ) 
                    from (
                    select 
                           p.ccodiplan CODIGO
                          ,p.cdescplan NOME
                      from solus.hssprede r, solus.hssredpl rp, solus.finpres ff, solus.hssplan p
                     where r.nnumeredea=rp.nnumeredea
                       and ff.nnumepres=r.nnumepres
                       and p.nnumeplan=rp.nnumeplan
                       and r.cpwebprede='S'
                       and ff.NNUMEPRES = x.NNUMEPRES 
                    ) x
                ),
                 KEY 'especialidade' IS (
                     select 
                         JSON_ARRAYAGG( json_object(x.* RETURNING CLOB) RETURNING CLOB ) 
                     from (
                     select 
                          EC.nnumeespec  id
                         ,EC.CNOMEESPEC  nome
                         ,ff.DCANCPRES    data_descrendenciamento
                     from solus.hssespre es, solus.finpres ff, solus.hssespec ec 
                     where es.nnumepres=ff.nnumepres
                     and ec.nnumeespec=es.nnumeespec
                     and es.cpublespre='S'
                     and es.NNUMEPRES = x.nnumepres
                    ) x
                ) RETURNING CLOB
             
         ) as result
  
    FROM (
    
      select 
          a.*
      from (
          SELECT
           F.NNUMEPRES ,  
           F.CNOMEPRES ,
           F.CFANTPRES ,
           F.CCGC_PRES ,
           E.CBAIRENDP ,
           E.CENDEENDP ,
           E.CNUMEENDP ,
           E.CCIDAENDP ,
           E.CESTAENDP ,
           E.CCEP_ENDP , 
           F.CFONEPRES ,
           F.CIDENPRES ,
           CAST(SOLUS.CALCULA_DISTACIA_COORD(
                       :latitude,
                       :longitude,
                       E.CLATIENDP,
                       E.CLONGENDP) AS NUMBER) distancia
          FROM SOLUS.FINPRES F
          JOIN SOLUS.HSSENDP E ON E.NNUMEPESS = F.NNUMEPESS                              
          WHERE
          1=1
          AND E.CLATIENDP IS NOT NULL
          AND E.CLONGENDP IS NOT null	
          AND F.NNUMEPRES IN (
                  SELECT 
                      DISTINCT FIN.NNUMEPRES
                  FROM
                      SOLUS.FINPRES  FIN
                      INNER JOIN SOLUS.HSSPREDE PR  ON PR.NNUMEPRES   = FIN.NNUMEPRES
                      INNER JOIN SOLUS.HSSREDPL R   ON R.NNUMEREDEA   = PR.NNUMEREDEA
                      INNER JOIN SOLUS.HSSPLAN  P   ON P.NNUMEPLAN    = R.NNUMEPLAN 
                      LEFT  JOIN SOLUS.HSSESPRE PRE ON PRE.NNUMEPRES  = FIN.NNUMEPRES 
                      LEFT  JOIN SOLUS.HSSESPEC ESP ON ESP.NNUMEESPEC = PRE.NNUMEESPEC
                  WHERE
                            1=1
                         ${filterMap.get('especialidade_id') || ''}
                         ${filterMap.get('plano_id') || ''}
                         ${filterMap.get('tipo_prestador_id') || ''}
                         ${filterMap.get('rede_id') || ''}
                         ${filterMap.get('prestador_bairro') || ''}
                         ${filterMap.get('prestador_uf') || ''}
                         ${filterMap.get('prestador_cidade') || ''}
                         -- AND PRE.CPUBLESPRE = 'S'
                 )
             ) a
             where 
             1=1
             ${filterMap.get('distancia') || ''}
             order by a.distancia asc
             ${filterMap.get('pagina') || ''}
             ${filterMap.get('limite') || ''}
             
         ) x
        `;
        await connection.execute(`ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,' `, [], connection);

        let queryPaginationDetails = `
        select 
        CEIL(total_num_rows / ${params.limite}) total_num_pages,
        total_num_rows as total_num_rows,
        ${pagina} as page
    from (
        select 
            row_number() OVER (ORDER BY a.distancia asc) rn,
                 COUNT(*) OVER () total_num_rows
        from (
            SELECT
             CAST(SOLUS.CALCULA_DISTACIA_COORD(
                        :latitude,
                        :longitude,
                         E.CLATIENDP,
                         E.CLONGENDP) AS NUMBER) distancia
            FROM SOLUS.FINPRES F
            JOIN SOLUS.HSSENDP E ON E.NNUMEPESS = F.NNUMEPESS                              
            WHERE
            1=1
            AND E.CLATIENDP IS NOT NULL
            AND E.CLONGENDP IS NOT null
            AND F.NNUMEPRES IN (
                    SELECT 
                        DISTINCT FIN.NNUMEPRES
                    FROM
                        SOLUS.FINPRES  FIN
                        INNER JOIN SOLUS.HSSPREDE PR  ON PR.NNUMEPRES   = FIN.NNUMEPRES
                        INNER JOIN SOLUS.HSSREDPL R   ON R.NNUMEREDEA   = PR.NNUMEREDEA
                        INNER JOIN SOLUS.HSSPLAN  P   ON P.NNUMEPLAN    = R.NNUMEPLAN 
                        LEFT  JOIN SOLUS.HSSESPRE PRE ON PRE.NNUMEPRES  = FIN.NNUMEPRES 
                        LEFT  JOIN SOLUS.HSSESPEC ESP ON ESP.NNUMEESPEC = PRE.NNUMEESPEC
                    WHERE
                              1=1
                        ${filterMap.get('especialidade_id') || ''}
                        ${filterMap.get('plano_id') || ''}
                        ${filterMap.get('tipo_prestador_id') || ''}
                        ${filterMap.get('rede_id') || ''}
                        ${filterMap.get('prestador_bairro') || ''}
                        ${filterMap.get('prestador_uf') || ''}
                        ${filterMap.get('prestador_cidade') || ''}
                    -- AND PRE.CPUBLESPRE = 'S'
                   )
               ) a
               where 
               1=1
               ${filterMap.get('distancia') || ''}
               order by a.distancia asc
               ${filterMap.get('pagina') || ''}
               ${filterMap.get('limite') || ''}
          ) x where rownum = 1
        `;

        let paginationResults = {total_num_pages: 0, total_num_rows: 0, page: 1, data: []};

        const paginationData = await this.db.findOne<any>(queryPaginationDetails, binds, connection);
        
        const resultPres = await this.db.queryJsonObj<any>(sql, binds, connection);

        paginationResults = {
                ...paginationResults, 
                ...paginationData, 
                data: resultPres
        };

        return paginationResults;
        
    }
}
