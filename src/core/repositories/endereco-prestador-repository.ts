import { Injectable } from "@nestjs/common";
import { TipoPrestadorModel } from "src/modules/guia-rede/models/tipo_prestador.model";
import { DbService } from "../database/services/db/db.service";

@Injectable()
export class EnderecoPrestadorRepository {

    constructor(private db: DbService) {}

    async getEstados() {
        let sql = `      
            select      
                e.CESTAENDP uf
            from solus.hssendp e
            where nnumepess in (
                select 
                    p.nnumepess 
                from solus.finpres f 
                inner join solus.hsspess p on p.nnumepess = f.nnumepess
            ) group by e.CESTAENDP
        `;
        return await this.db.query(sql);
    }

    async getCidadeByEstado(uf: string) : Promise<TipoPrestadorModel[]> {
        let sql = `            
            select 
                e.CCIDAENDP cidade
            from solus.hssendp e
            where nnumepess in (
                select 
                    p.nnumepess 
                from solus.finpres f 
                inner join solus.hsspess p on p.nnumepess = f.nnumepess
            )
            and e.cestaendp = :uf
            group by e.CCIDAENDP
        `;
        return await this.db.query<TipoPrestadorModel>(sql, {uf: uf});
    }

    async getBairrosByCidade(cidade: string) : Promise<TipoPrestadorModel[]> {
        let sql = `            
            select 
                e.CBAIRENDP bairro
            from solus.hssendp e
            where nnumepess in (
                select 
                    p.nnumepess 
                from solus.finpres f 
                inner join solus.hsspess p on p.nnumepess = f.nnumepess
            )
            and e.CCIDAENDP = :cidade
            group by e.CBAIRENDP
        `;
        return await this.db.query<TipoPrestadorModel>(sql, {cidade: cidade});
    }

    }
