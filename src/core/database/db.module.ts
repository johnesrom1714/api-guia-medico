import { Module } from '@nestjs/common';
import { DbService } from './services/db/db.service';
import 'dotenv/config';
@Module({
  providers: [DbService],
  exports: [DbService],
})
export class DbModule {}
