import { Injectable } from '@nestjs/common';
import * as oracledb from 'oracledb';

oracledb.autoCommit = false;
oracledb.fetchArraySize = 100;
oracledb.fetchAsString = [oracledb.CLOB];
oracledb.fetchAsBuffer = [oracledb.BLOB];

@Injectable()
export class DbService {
  public oracle: any;

  constructor() {
    this.oracle = oracledb;
    this.createPool();
  }

  private async createPool() {
    try {
      await oracledb.createPool({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        connectString: `${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`,
        poolIncrement: 3,
        poolMax: 25,
        poolMin: 3,
        poolPingInterval: 1,
        poolTimeout: 60,
      });
      console.log('Connection pool started');

      //encerra o pool quando encerra a aplicação
      process
        .once('SIGTERM', this.closePoolAndExit)
        .once('SIGINT', this.closePoolAndExit);
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    }
  }

  async open() {
    return await oracledb.getConnection();
  }

  async close(connection: any) {
    try {
      if (!connection) {
        console.log('Connection is null');
        return;
      }

      await connection.close();
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    }
  }

  async commitAndClose(connection) {
    try {
      if (!connection) {
        console.log('Connection is null');
        return;
      }

      // Put the connection back in the pool
      await connection.commit();

      await connection.close();
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    }
  }

  async rollback(connection) {
    try {
      if (!connection) {
        console.log('Connection is null');
        return;
      }

      // Put the connection back in the pool
      await connection.rollback();
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    }
  }

  async rollbackAndClose(connection) {
    try {
      if (!connection) {
        console.log('Connection is null');
        return;
      }

      // Put the connection back in the pool
      await connection.rollback();

      await connection.close();
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    }
  }

  async execute<T = any>(
    sql: string,
    binds: any = [],
    connection: any = null,
  ): Promise<T> {
    let isOpenTransaction = true;
    try {
      // Get a connection from the default pool
      if (!connection) {
        connection = await this.open();
        isOpenTransaction = false;
      }

      const options: oracledb.ExecuteOptions = {
        outFormat: oracledb.OBJECT,
        maxRows: 1000,
        dir: oracledb.BIND_IN,
      };

      const result = await connection.execute(sql, binds, options);

      return result;
    } catch (err) {
      console.log(err.stack);
      throw new Error(err);
    } finally {
      if (!isOpenTransaction) {
        this.commitAndClose(connection);
      }
    }
  }

  async query<T = any>(
    sql: string,
    binds: any = [],
    connection: any = null,
  ): Promise<Array<T>> {
    let isOpenTransaction = true;
    const isLogging = Boolean(JSON.parse(process.env.DB_LOGGING));
    if (isLogging) {
      console.log(`query ${new Date()} log:`);
      console.log(sql);
      binds && console.log('BINDS ==>', binds);
    }
    try {
      // Get a connection from the default pool
      if (!connection) {
        connection = await this.open();
        isOpenTransaction = false;
      }

      const options = {
        outFormat: oracledb.OBJECT,
        maxRows: 1000,
        dir: oracledb.BIND_IN,
      };

      const result = await connection.execute(sql, binds, options);

      let rows = [];
      if (result && result.rows && result.rows.length > 0) {
        rows = result.rows.map((one) => {
          const newValues = {};
          Object.keys(one).forEach(
            (key) => (newValues[key.toLowerCase()] = one[key]),
          );
          return newValues;
        });
      }

      return rows;
    } catch (err) {
      console.log('Has db errors');
      console.log(err.stack);
      throw new Error(err);
    } finally {
      if (!isOpenTransaction) {
        this.commitAndClose(connection);
      }
    }
  }

  async queryJsonAgg<T = any>(
    sql: string,
    binds: any = [],
    connection: any = null,
  ): Promise<Array<T>> {
    let isOpenTransaction = true;
    const isLogging = Boolean(JSON.parse(process.env.DB_LOGGING));
    if (isLogging) {
      console.log(`query ${new Date()} log:`);
      console.log(sql);
      binds && console.log('BINDS ==>', binds);
    }
    try {
      // Get a connection from the default pool
      if (!connection) {
        connection = await this.open();
        isOpenTransaction = false;
      }

      const options = {
        outFormat: oracledb.JSON,
        maxRows: 1000,
        dir: oracledb.BIND_IN,
      };

      const result = await connection.execute(sql, binds, options);
      console.log(result);
      let rows = [];
      if (result && result.rows && result.rows.length > 0) {
        rows = JSON.parse(result.rows[0]);
      }
      return rows;
    } catch (err) {
      console.log('Has db errors');
      console.log(err.stack);
      throw new Error(err);
    } finally {
      if (!isOpenTransaction) {
        this.commitAndClose(connection);
      }
    }
  }

  async queryJsonObj<T = any>(
    sql: string,
    binds: any = [],
    connection: any = null,
  ): Promise<Array<T>> {
    let isOpenTransaction = true;
    const isLogging = Boolean(JSON.parse(process.env.DB_LOGGING));
    if (isLogging) {
      console.log(`query ${new Date()} log:`);
      console.log(sql);
      binds && console.log('BINDS ==>', binds);
    }
    try {
      // Get a connection from the default pool
      if (!connection) {
        connection = await this.open();
        isOpenTransaction = false;
      }

      const options = {
        outFormat: oracledb.JSON,
        maxRows: 1000,
        dir: oracledb.BIND_IN,
      };

      const result = await connection.execute(sql, binds, options);
      console.log(result);
      let rows = [];
      if (result && result.rows && result.rows.length > 0) {
        rows = result.rows.map(x => JSON.parse(x));
      }
      return rows;
    } catch (err) {
      console.log('Has db errors');
      console.log(err.stack);
      throw new Error(err);
    } finally {
      if (!isOpenTransaction) {
        this.commitAndClose(connection);
      }
    }
  }

  async findOne<T = any>(sql: string, binds: any = [], connection: any = null) {

    let isOpenTransaction = true;
  
    try {

      if (!connection) {
        connection = await this.open();
        isOpenTransaction = false;
      }

      const one = await this.query<T>(sql, binds, connection);
      if (one.length >= 1) {
        return one[0];
      }
      return null;
    }catch(ex){
        
    }
  }

  async closePoolAndExit() {
    console.log('\nTerminating');
    try {
      await oracledb.getPool().close(10);
      console.log('Pool closed');
      process.exit(0);
    } catch (err) {
      console.error(err.message);
      process.exit(1);
    }
  }
}
