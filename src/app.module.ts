import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DbModule } from './core/database/db.module';
import { DbService } from './core/database/services/db/db.service';
import { PrestadorRepository } from './core/repositories/prestador-repository';
import { GuiaRedeController } from './modules/guia-rede/controllers/guia-rede/guia-rede.controller';
import { GuiaRedeModule } from './modules/guia-rede/guia-rede.module';

@Module({
  imports: [DbModule, GuiaRedeModule],
  controllers: [
    AppController,
    GuiaRedeController
  ],
  providers: [
    PrestadorRepository
  ],
})
export class AppModule {}
