import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';
import { GuiaRedeModule } from './modules/guia-rede/guia-rede.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  useContainer(app.select(GuiaRedeModule), { fallbackOnErrors: true });
  
  app.enableCors();
  
  const port: number = parseInt(process.env.PORT) || 80;
  const config = new DocumentBuilder()
    .setTitle('API Consulta Rede')
    .setDescription('API para consultar rede no SOLUS')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/docs/', app, document);
  
  await app.listen(port);
}
bootstrap();
