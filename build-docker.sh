#!/bin/bash

echo 'First arg:' $1

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 0
fi

docker rm -f guia-medico-$1

if [ "$1" ==  "prod" ]
  then
    docker build -f Dockerfile.prod -t healthdev/guia-medico-"$1" .
fi


if [ "$1" ==  "homo" ]
  then
    docker build -f Dockerfile.homo -t healthdev/guia-medico-"$1" .
fi

if [ "$1" ==  "dev" ]
  then
    docker build -f Dockerfile.dev -t healthdev/guia-medico-"$1" .
fi


docker run -dit --name guia-medico-"$1" --add-host=host.docker.internal:host-gateway -p 3000:80 healthdev/guia-medico-"$1"

# docker rm -f guia-medico
# doc   
# docker run -dit --name guia-medico --add-host=host.docker.internal:host-gateway -p 3000:8080 healthdev/guia-medico


