<?php
  require_once("../comum/autoload.php");

  $seg->secureSessionStart();

  if ($_SESSION['sistema'] <> 'Rede') {
    require_once('../comum/sessao.php');
  }

  $bd =  new Oracle();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  $_SESSION['titulo'] = "REDE CREDENCIADA";

  if (!isset($_SESSION['id_contrato']))
    $_SESSION['id_contrato'] = 0;

  if (!isset($_SESSION['id_locacao']))
    $_SESSION['id_locacao'] = 0;

  require_once("../comum/layout.php");
  $tpl->addFile("CONTEUDO","../comum/redeCredenciada.htm");

  //PEGANDO ID DO USUÁRIO VINDO DO APLICATIVO
  $tpl->IDUSER = '';
  $tpl->LAT = '';
  $tpl->LNG = '';

  if (isset($_GET['idSessao']))
    $tpl->ID_SESSAO = $_GET['idSessao'];

  if ($_SESSION['apelido_operadora'] == 'saudemed')
    $tpl->block("SAUDEMED");

  $registros = 20;

  if (isset($_GET['pg']))
    $pg = $_GET['pg'];
  else
    $pg = 0;

  $inicio = $pg * $registros;

  $filtros = array();


//############################
//DEBUG-LIVSAUDE #############
//echo '<pre>';  print_r($_POST); echo '</pre>'; die();
//############################


  if (isset($_POST['listar']) or isset($_POST['imprimir'])) {
    $filtros['tipo_prestador']   = $seg->antiInjection($_POST['tipo_prestador']);
    $filtros['estado']           = $seg->antiInjection($_POST['estado']);
    $filtros['cidade']           = $seg->antiInjection($_POST['cidade']);
    $filtros['tipoRede']         = $seg->antiInjection($_POST['tipoRede']);
    $filtros['especialidade']    = $seg->antiInjection($_POST['especialidade']);
    $filtros['area']             = $seg->antiInjection($_POST['area']);
    $filtros['graduacao']        = $seg->antiInjection($_POST['graduacao']);
    $filtros['s_descredenciado'] = $seg->antiInjection($_POST['s_descredenciado']);
    $filtros['s_corpoClinico']   = $seg->antiInjection($_POST['s_corpoClinico']);
    $filtros['urg_emergencia'] = $seg->antiInjection($_POST['urg_emergencia']);


    $filtros['bairro']         = $seg->antiInjection($_POST['bairro']);

    if (isset($_POST['plano']))
      $filtros['plano']        = $seg->antiInjection($_POST['plano']);
    else
      $filtros['plano']        = '';

    $filtros['nome_prestador'] = $seg->antiInjection($_POST['nome_prestador']);
    $filtros['inicio']         = 0;
    $pg                        = 0;
    $filtros['executa']        = 'N';
    $_SESSION['filtrosRede']   = $filtros;
  }
  else if (isset($_POST['diashoras'])) {
    echo $util->redireciona('Impdiashoras.php?idSessao='.$_GET['idSessao'],'S');
  }
  else if (isset($_SESSION['filtrosRede'])) {
    $filtros = $_SESSION['filtrosRede'];
    $filtros['executa']          = 'S';
    $filtros['inicio']           = $inicio;
  }
  else {
    $filtros['tipo_prestador']   = '';
    $filtros['tipoRede']         = '';
    $filtros['estado']           = '';
    $filtros['cidade']           = '';
    $filtros['especialidade']    = '';
    $filtros['area']             = '';
    $filtros['bairro']           = '';
    $filtros['plano']            = '';
    $filtros['nome_prestador']   = '';
    $filtros['inicio']           = 0;
    $filtros['executa']          = 'N';
    $filtros['graduacao']        = '';
    $filtros['s_descredenciado'] = '';
    $filtros['s_corpoClinico']   = '';
    $filtros['urg_emergencia'] = '';
  }

  $tipo_prestador   = $filtros['tipo_prestador'];
  $estado           = $filtros['estado'];
  $tipoRede         = $filtros['tipoRede'];
  $cidade           = $filtros['cidade'];
  $especialidade    = $filtros['especialidade'];
  $area_prestador   = $filtros['area'];
  $bairro           = $filtros['bairro'];
  $plano            = $filtros['plano'];
  $nome_prestador   = $filtros['nome_prestador'];
  $inicio           = $filtros['inicio'];
  $graduacao        = $filtros['graduacao'];
  $s_descredenciado = $filtros['s_descredenciado'];
  $s_corpoClinico   = $filtros['s_corpoClinico'];
  $urg_emergencia = $filtros['urg_emergencia'];


  if (($_SESSION['apelido_operadora'] <> 'affego') and ($_SESSION['apelido_operadora'] <> 'vitallis'))
    $tpl->block("MOSTRA_AREA");

  $sql =  new Query($bd);

  /** Select tipo prestador **/
  if (($_SESSION['apelido_operadora'] <> 'saudemed') and
      ($_SESSION['apelido_operadora'] <> 'affego')) {

    if ($_SESSION['apelido_operadora'] == 'vitallis')
      $tipos = array('C','A','I','D','J','T','3','S','2','*','H','1','X','L','M','U','P','Q','8');
    else if ($_SESSION['apelido_operadora'] == 'universal')
      $tipos = array('C','A','E','H','L','M','P','N');
    else if ($_SESSION['apelido_operadora'] == 'unimedJundiai')
      $tipos = array('C','M','J','T','S','6','H','L','P','Q','4');
    else if ($_SESSION['apelido_operadora'] == 'sampes')
      $tipos = array('H','2','L','M','N');
    else {
      /** Select tipos **/
      if ($_SESSION['apelido_operadora'] == 'sampmg'){
        $txt = "SELECT DISTINCT CGRUPPRES
                  FROM FINPRES
                 WHERE CCREDPRES IN ('S','O')
                   AND CGRUPPRES NOT IN ('B','F','D','K','O','R','V','W')
                 ORDER BY 1";
      }
      else {
        $txt = "SELECT DISTINCT CGRUPPRES, RETORNA_NOME_GRUPO_PRESTADOR(CGRUPPRES) GRUPO
                  FROM FINPRES
                 WHERE ( (CCREDPRES IN ('S','O'))  OR (CCREDPRES IN ('N','M') AND (DCANCPRES + NVL(NDVISPRES, 0)) >= TRUNC(SYSDATE)) )
                 ORDER BY 2";

      }

      $sql->executeQuery($txt);

      while (!$sql->eof()) {
        $tipos[] = $sql->result("CGRUPPRES");
        $sql->next();
      }
    }

    foreach($tipos as $t) {
      $tpl->TIPO_ID = $t;
      $tpl->TIPO_DESCRICAO = $func->tipoPrestador($bd,$t);

      if ($tipo_prestador == $t)
        $tpl->REDE_TIPO = 'selected';
      else
        $tpl->REDE_TIPO = '';

      $tpl->block("REDE_ITEM_TIPO_PRESTADOR");
    }

    $tpl->block("MOSTRA_TIPO_PRESTADOR");
  }

  if (($_SESSION['apelido_operadora'] == 'vitallis') or ($_SESSION['apelido_operadora'] == 'sampmg') ) {
    $tpl->block("CHANGE_REDE");

    /** Select Rede **/
    $txt = "SELECT NNUMEREDEA,CNOMEREDEA
              FROM HSSREDEA
             WHERE CSITUREDEA <> 'C'
               AND CWEB_REDEA = 'S'
               AND NNUMEREDEA IN(SELECT NNUMEREDEA FROM HSSREDPL)
             ORDER BY 2";

    $sql->executeQuery($txt);

    if ($sql->count() > 0 ) {
      while (!$sql->eof()) {
        $tpl->REDE_ID = $sql->result("NNUMEREDEA");
        $tpl->REDE_NOME = $sql->result("CNOMEREDEA");
        $tpl-> block("ITEM_REDE");
        $sql->next();
      }
      $tpl->block("REDE");
    }
  }
  else {
    $tpl->block("CHANGE_PLANO");

    if ($_SESSION['sistema'] == 'Rede') {
      /** Select Plano **/
      if($_SESSION['apelido_operadora'] == 'UnimedLestePaulista'){
        $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN, HSSPLAN.CCODIPLAN || ' - ' || NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) || ' - ' || DECODE(HSSPLAN.CNATUPLAN,1,'Individual Familiar',3,'Coletivo por Adesão',4,'Coletivo Empresarial',HSSPLAN.CNATUPLAN)|| ' - Registro ANS: ' || NVL(HSSTITU.CSCPATITU,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN),HSSPLAN.CANTIPLAN)) || ' - ATIVO' CDESCPLAN
                  FROM HSSPLAN,HSSTITU
                 WHERE NVL(CSITUPLAN,'A') = 'A'
                   AND HSSPLAN.CWEB_PLAN ='S'
                   AND HSSPLAN.CEXRWPLAN = 'S'
                   AND HSSTITU.CSITUTITU = 'A'
                   AND 0 < (SELECT COUNT(*) FROM HSSUSUA WHERE CSITUUSUA = 'A' AND NNUMETITU = HSSTITU.NNUMETITU)
                   AND HSSPLAN.NNUMEPLAN = HSSTITU.NNUMEPLAN
                 ORDER BY 2";
      }
      else if($_SESSION['apelido_operadora'] == 'camboriuSaude'){
        $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN, CCODIPLAN || ' - ' || CNPCAPLAN || ' - ' || DECODE(CNATUPLAN,1,'Individual Familiar',3,'Coletivo por Adesão',4,'Coletivo Empresarial',CNATUPLAN)|| ' - Registro ANS: ' || NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) || ' - ATIVO' CDESCPLAN
                  FROM HSSPLAN,HSSREDPL
                 WHERE NVL(CSITUPLAN,'A') = 'A'
                   AND HSSPLAN.NNUMEPLAN = HSSREDPL.NNUMEPLAN
                 ORDER BY 2";
      }
      else if($_SESSION['apelido_operadora'] == 'freelife'){
        $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN, CCODIPLAN || ' - ' || NVL(CBOLEPLAN,CDESCPLAN) || ' - ' || DECODE(CNATUPLAN,3,'Coletivo por Adesão',4,'Coletivo Empresarial','Individual Familiar') || ' - Registro ANS: ' || NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) CDESCPLAN
                  FROM HSSPLAN
                 WHERE NVL(CSITUPLAN,'A') <> 'C'
                   AND HSSPLAN.CWEB_PLAN ='S'
                 ORDER BY CDESCPLAN";
      }
      else{
        $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN, CCODIPLAN || ' - ' || NVL(CNPCAPLAN,CDESCPLAN) || ' - ' || DECODE(CNATUPLAN,4,'Coletivo por Adesão',3,'Coletivo Empresarial','Individual Familiar') || ' - Registro ANS: ' || NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) CDESCPLAN
                  FROM HSSPLAN
                 WHERE NVL(CSITUPLAN,'A') <> 'C'
                   AND HSSPLAN.CWEB_PLAN ='S'
                   AND CEXRWPLAN = 'S'
                 ORDER BY CDESCPLAN";
      }
      $sql->executeQuery($txt);

      if ($sql->count() > 0 ) {
        while (!$sql->eof()) {
          $tpl->PLANO_ID = $sql->result("NNUMEPLAN");
          $tpl->PLANO_NOME = $sql->result("CDESCPLAN");
          $tpl-> block("ITEM_PLANO");
          $sql->next();
        }
        $tpl->block("PLANO");
      } else
        $plano = -1;

      $txt1 = "SELECT NNUMEREDEA, CNOMEREDEA, CSITUREDEA
                 FROM HSSREDEA
                WHERE CSITUREDEA ='A'
                  AND CWEB_REDEA = 'S'
                ORDER BY CNOMEREDEA";
    } else {
      $plano = -1;

      $txt1 = "SELECT NNUMEREDEA, CNOMEREDEA, CSITUREDEA
                FROM HSSREDEA
                WHERE CSITUREDEA ='A'
                ORDER BY CNOMEREDEA";
    }

    $sql->executeQuery($txt1);
    $result = $sql ->getResults();

    if ($sql->count() > 0){
      $i=0;

      foreach ($result['NNUMEREDEA'] as $key => $valor){
        $novo[$i]['NNUMEREDEA']=$valor;
        $novo[$i]['CNOMEREDEA']=$result['CNOMEREDEA'][$i];
        $i++;
      }

      foreach ($novo as $key => $valor){
        $tpl->REDE_ID1 = $valor['NNUMEREDEA'];
        $tpl->REDE_NOME1 = $valor['CNOMEREDEA'];
        $tpl->block("ENDBUSCAREDE2");

      }
      $tpl->block("REDE2");
    }
  }


  //Select para apas
  if ($_SESSION['apelido_operadora'] == 'apasItapetininga'){
    $estado_setado = "AND CESTAPRES = 'SP'  ";
    $estado_setado2 = "AND HSSEPRES.CESTAEPRES = 'SP'  ";
  }else{
    $estado_setado = " ";
    $estado_setado2 = " ";
  }
  
  if(!empty($_SESSION['endereco']['uf']) && empty($estado)){
    $estado = $_SESSION['endereco']['uf'];
  }
/*
  if(!empty($_POST['planoBusca'])){
    if(!empty($_POST['estadoBusca'])){
      $estado = $_POST['estadoBusca'];
    }
    $txt = "SELECT DISTINCT cestaabgmu as CESTAPRES from hssabgmu WHERE nnumeplan = :plano ORDER BY 1 ASC ";
    $sql->addParam(":plano",$_POST['planoBusca']);
  }else{
    */
    /** Select estado - no prestador é preciso pegar Como principal a UF do Conselho médico, pois na busca é baseado neste campo **/
    $txt = " SELECT DISTINCT TRIM(NVL(CUFCRPRES,CESTAPRES)) CESTAPRES
             FROM FINPRES
            WHERE CCREDPRES IN ('S','O')
              AND CESTAPRES IS NOT NULL
              AND NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                  FROM HSSESPRE,HSSAAPRE
                                 WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES
                                   AND HSSESPRE.CPUBLESPRE = 'S'
                                   AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                   AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+))"
             .$estado_setado.
           "UNION
           SELECT DISTINCT TRIM(CESTAEPRES) CESTAPRES
             FROM HSSEPRES,FINPRES
            WHERE HSSEPRES.CSITUEPRES = 'S'
              AND HSSEPRES.CTIPOEPRES = 'A'
              AND HSSEPRES.CESTAEPRES IS NOT NULL
              AND HSSEPRES.NNUMEPRES = FINPRES.NNUMEPRES
              AND CCREDPRES IN ('S','O')"
              .$estado_setado2.
           "ORDER BY 1 ";
  //}
  $sql->executeQuery($txt);

  while (!$sql->eof()) {
    if ($_SESSION['apelido_operadora'] == 'sampmg') {
      if (($sql->result("CESTAPRES") == 'ES') || ($sql->result("CESTAPRES") == 'MG')){
        $tpl->ESTADO_ID = $sql->result("CESTAPRES");
        $tpl->ESTADO_NOME = $func->nomeEstado($sql->result("CESTAPRES"));

        if ($estado == $sql->result("CESTAPRES"))
          $tpl->REDE_ESTADO = 'selected';
        else
          $tpl->REDE_ESTADO = '';

        $tpl-> block("REDE_ITEM_ESTADO");
      }
    }
    else if ($_SESSION['apelido_operadora'] == 'unimedJundiai') {
      if ($sql->result("CESTAPRES") == 'SP') {
        $tpl->ESTADO_ID = $sql->result("CESTAPRES");
        $tpl->ESTADO_NOME = $func->nomeEstado($sql->result("CESTAPRES"));

        if ('SP' == $sql->result("CESTAPRES")) {
          $tpl->REDE_ESTADO = "selected";
          $estado = $sql->result("CESTAPRES");
        }
        else
          $tpl->REDE_ESTADO = "";

        $tpl-> block("REDE_ITEM_ESTADO");
      }
    }
    else {
      $tpl->ESTADO_ID = $sql->result("CESTAPRES");
      $tpl->ESTADO_NOME = $func->nomeEstado($sql->result("CESTAPRES"));

      if ($estado == $sql->result("CESTAPRES"))
        $tpl->REDE_ESTADO = 'selected';
      else
        $tpl->REDE_ESTADO = '';

      $tpl-> block("REDE_ITEM_ESTADO");
    }
    $sql->next();
  }


  /** Select especialidade **/
  $sql->clear();
  $txt = "SELECT CNOMEESPEC,NNUMEESPEC
            FROM HSSESPEC
           WHERE CSITUESPEC = 'A'
             AND CPUBLESPEC = 'S'
           ORDER BY CNOMEESPEC";
  $sql->executeQuery($txt);

  while (!$sql->eof()) {
    $tpl->ESPECIALIDADE_ID = $sql->result("NNUMEESPEC");
    $tpl->ESPECIALIDADE_DESCRICAO = $sql->result("CNOMEESPEC");

    if ($especialidade == $sql->result("NNUMEESPEC"))
      $tpl->REDE_ESPECIALIDADE = 'selected';
    else
      $tpl->REDE_ESPECIALIDADE = '';

    $tpl-> block("REDE_ITEM_ESPECIALIDADE");
    $sql->next();
  }

  /** Select regiao prestador **/
  
  $sql->clear();
  $txt = "SELECT NNUMEREGIA,CDESCREGIA
            FROM HSSREGIA
           ORDER BY CDESCREGIA";
  $sql->executeQuery($txt);

  while (!$sql->eof()) {
    $tpl->REGIAO_ID = $sql->result("NNUMEREGIA");
    $tpl->REGIAO_DESCRICAO = $sql->result("CDESCREGIA");

    $tpl-> block("REGIAO_ITEM_PRESTADOR");
    $sql->next();
  }
  $tpl-> block("REGIAO_PRESTADOR");
  

  $nome_prestador = strtoupper($nome_prestador);


  if(!empty($_SESSION['plano']['id']))
      $plano = $_SESSION['plano']['id'];

  if(!empty($_SESSION['endereco']['uf']))
      $estado = $_SESSION['endereco']['uf'];  
      
  if(!empty($_SESSION['endereco']['cidade']))
      $cidade = $_SESSION['endereco']['cidade'];



  if (isset($_POST['planoBusca']))
    $plano = $seg->antiInjection($_POST['planoBusca']);
  if (isset($_POST['estadoBusca']))
    $estado = $seg->antiInjection($_POST['estadoBusca']);
  if (isset($_POST['cidadeBusca']))
    $cidade = $seg->antiInjection($_POST['cidadeBusca']);

  if (isset($_POST['nomeBusca'])){
    $tpl->USUARIO_BUSCA = 'Olá '.$seg->antiInjection($_POST['nomeBusca']);
    $tpl->block("NOME_BUSCA");
  }

  
  $tpl->SEL_NOME_PRESTADOR  = $nome_prestador;
  $tpl->SEL_TIPO_PRESTADOR  = $tipo_prestador;
  $tpl->SEL_ESTADO          = $estado;
  $tpl->SEL_CIDADE          = $cidade;
  $tpl->SEL_BAIRRO          = $bairro;
  $tpl->SEL_ESPECIALIDADE   = $especialidade;
  $tpl->SEL_AREA_PRESTADOR  = $area_prestador;
  $tpl->SEL_PLANO_PRESTADOR = $plano;
  $tpl->SEL_REDE            = $tipoRede;
  $tpl->SEL_GRADUACAO       = $graduacao;
  $tpl->HABILITA_FILTRO     = $seg->permissaoOutros($bd,'WEBEMPRESADESABILITAFILTROSOMENTEPRESTADORESDESCREDENCIADOS',false);
  $tpl->CADASTRO_OPERADORA  = $_SESSION['apelido_operadora'];
  $tpl->CORPO_CHECKED  = 'checked';

  if (($_SESSION['apelido_operadora'] <> 'UnimedLestePaulista') and ($_SESSION['apelido_operadora'] <> 'unimedJundiai') and ($_SESSION['apelido_operadora'] <> 'vitallis')) {
    $tpl->block("MOSTRA_BOTAO_DIAS_ATENDIMENTO");
  }

  if ($_SESSION['apelido_operadora'] == 'hospitalar') {
    $tpl->block("GUIA_MEDICO");
  }
  else if ($sql->result("CAPELEMPR") == 'sampes')
    $tpl->block("GUIA_MEDICO_SAMP");
  else {
    //$tpl->block("IMPRIMIR");
  }

  if ($_SESSION['sistema'] <> 'Rede') {
    $tpl->block("MOSTRA_MENU");
  }

  $bd->close();
  $tpl->show();

?>